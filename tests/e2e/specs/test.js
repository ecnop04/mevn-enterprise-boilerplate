// For authoring Nightwatch tests, see
// https://nightwatchjs.org/guide

module.exports = {
  "default e2e tests": browser => {
    browser
      .init()
      .waitForElementVisible("#app")
      .assert.elementPresent(".hello")
      .assert.containsText("h1", "Welcome to Your Vue.js App")
      .assert.elementCount("img", 1)
      .end();
  },

  "example e2e test using a custom command": browser => {
    browser
      .openHomepage()
      .assert.elementPresent(".hello")
      .end();
  },

  "about page test": browser => {
    browser
      .url(browser.launchUrl + 'about')
      .waitForElementVisible("#app")
      .assert.elementPresent(".about")
      .assert.containsText("h1", "Profile Information")
      .end();
  },


};
